$(function () {
    $('.dd').nestable();

    $('.dd').on('change', function () {
        var $this = $(this);
        // var serializedData = window.JSON.stringify($($this).nestable('serialize'));
        var serializedData = $($this).nestable('serialize');
        var data = [];

        $.each(serializedData, function(i, val) {
        	var data  = [];
        	val.order = i + 1;
        	data.push({'task': val});

        	$.ajax({
			    url: '/tasks/'+val.id,
			    type: 'PUT',
			    contentType: "application/json",
			    data: window.JSON.stringify(data[0]),
			    success: function(result) {
			        
			    }
			});
        });

        $this.parents('div.body').find('textarea').val(serializedData);
    });
});