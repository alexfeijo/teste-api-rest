<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Task;

class TaskTest extends TestCase
{
    public function testGetAll()
    {
        $response = $this->json('GET', '/tasks');

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => []
            ]);
    }

    public function testShowTask()
    {
    	$task 		= factory(Task::class)->create();
    	$response 	= $this->json('GET', '/tasks/'.$task->id);

    	$response
    			->assertStatus(200)
	            ->assertJson([
	                'data' => []
	            ]);
    }
}
