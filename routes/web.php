<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'HomeController@index']);

Route::get('/tasks', ['uses' => 'TaskController@task']);
Route::post('/tasks', ['uses' => 'TaskController@store']);
Route::put('/tasks/{id}', ['uses' => 'TaskController@update']);
Route::delete('/tasks/{id}', ['uses' => 'TaskController@destroy']);
Route::get('/tasks/{id}', ['uses' => 'TaskController@show']);

