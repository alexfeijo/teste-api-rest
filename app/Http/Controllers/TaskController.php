<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Validator;

class TaskController extends Controller
{
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function task()
    {
        return response()->json([
                            'data' => [
                                'tasks' => $this->task->orderBy('order', 'DESC')->get()
                                ]
                            ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->task, $this->task->rules, $this->task->messages);

        if ($validator->fails()) {
            return response()->json([
                    'data' => [
                        'messages' => $validator->errors()
                        ]
                    ], 422);
        }

        $this->task->fill($request->task);
        $this->task->order = $this->task->get()->last()->id + 1;
        $this->task->save();
        
        return response()->json([
                            'data' => [
                                'task' => $this->task
                                ]
                            ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = $this->task->find($id);
        if ($task) {
            return response()->json([
                            'data' => [
                                'task'      => $task,
                                'message'   => 'Requisição completa'
                                ]
                            ], 200);
        } else {
            return response()->json([
                            'data' => [
                                'message' => 'Tarefa solicitada não existe'
                                ]
                            ], 422);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->task, $this->task->rules, $this->task->messages);

        if ($validator->fails()) {
            return response()->json([
                    'data' => [
                        'messages' => $validator->errors()
                        ]
                    ], 422);
        }
        $task = $this->task->find($id);

        if (!$task) {
            return response()->json([
                    'data' => [
                        'messages' => 'Tarefa não encontrada.'
                        ]
                    ], 422);
        }

        $task->fill($request->task);
        $task->save();
        
        return response()->json([
                            'data' => [
                                'task' => $task
                                ]
                            ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = $this->task->find($id);

        if (!$task) {
            return response()->json([
                    'data' => [
                        'messages' => 'Tarefa não encontrada.'
                        ]
                    ], 422);
        }

        $task->destroy();

        return response()->json([
                            'data' => [
                                'mesage' => 'Tarefa excluída com sucesso'
                                ]
                            ], 200);
    }
}
