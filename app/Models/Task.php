<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = "tasks";

    protected $fillable = [
        'title',
        'description',
        'order'
    ];

    public $rules = [
    	"title" => 'required'
    ];

    public $messages = [
    	"title.required" => "O título é obrigatório"
    ];
}
