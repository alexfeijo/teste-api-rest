<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Manager Task</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('assets/admin-materialdesign/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('assets/admin-materialdesign/plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('assets/admin-materialdesign/plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{{ asset('assets/admin-materialdesign/plugins/morrisjs/morris.css') }}" rel="stylesheet" />
    
    <link href="{{ asset('assets/admin-materialdesign/plugins/nestable/jquery-nestable.css')}}" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="{{ asset('assets/admin-materialdesign/css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('assets/admin-materialdesign/css/themes/all-themes.css') }}" rel="stylesheet" />
</head>

<body class="theme-indigo">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Por favor aguarde...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="/">Gerenciador de tarefas</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{ asset('assets/admin-materialdesign/images/user.png')}}" width="48" height="48" alt="Alexandro Flores Feijó" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Alexandro Flores Feijó</div>
                    <div class="email">alexandroffeijó@gmail.com</div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">Menu de navegação</li>
                    <li class="active">
                        <a href="index.html">
                            <i class="material-icons">assignment</i>
                            <span>Tarefas</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright"></div>
                <div class="version"></div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

       <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Gerenciador de tarefas</h2>
            </div>
            <div class="row clearfix">
            </div>
            <!-- Condensed Table -->
            <!-- Draggable Handles -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DRAGGABLE HANDLES
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="clearfix m-b-20">
                                <div class="dd nestable-with-handle">
                                    <ol class="dd-list">
                                        @foreach($task as $task)
                                            <li class="dd-item dd3-item" 
                                            data-order="{{ $task->order }}"
                                            data-id="{{ $task->id }}"
                                            data-title="{{ $task->title }}">
                                                <div class="dd-handle dd3-handle"></div>
                                                <div class="dd3-content">
                                                    <p><strong>{{ $task->title }}</strong></p>
                                                    <p><small>{{ $task->description }}</small></p>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ol>
                                </div>
                            </div>
                            <b>Output JSON</b>
                            <textarea cols="30" rows="3" class="form-control no-resize" readonly>[{"id":13},{"id":14},{"id":15,"children":[{"id":17},{"id":16},{"id":18}]}]</textarea>
                        </div>
                    </div>
                </div>
            <!-- #END# Draggable Handles -->
            </div>
        </div>
    </section>

     <!-- Jquery Core Js -->
    <script src="{{ asset('assets/admin-materialdesign/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('assets/admin-materialdesign/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('assets/admin-materialdesign/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('assets/admin-materialdesign/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('assets/admin-materialdesign/plugins/node-waves/waves.js')}}"></script>

    <!-- Jquery Nestable -->
    <script src="{{ asset('assets/admin-materialdesign/plugins/nestable/jquery.nestable.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/admin-materialdesign/js/admin.js')}}"></script>
    <script src="{{ asset('assets/admin-materialdesign/js/pages/ui/sortable-nestable.js')}}"></script>

    <!-- Demo Js -->
    <!-- <script src="{{ asset('assets/admin-materialdesign/js/demo.js')}}"></script> -->
</body>

</html>