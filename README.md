# README #

Este Projeto foi desenvolvido com * [Laravel](https://laravel.com/).

### Api REST de tarefas, Create, Read, Update, Delete ###

* Clone este repositório
* Crie um virtualhost apontando para a pasta public/, o Laravel trabalha sempre a partir dela.
* Caso necessário de permissões 777 na pasta storage, e 755 na pasta public
* Edite o arquivo .env colocando seus dados do banco de dados
* Rode o comando php artisan migrate, ele executará as migrations para criar as tabelas necessárias. Caso prefira existe um arquivo tasks.sql com o banco
* Após isso estará rodando no virtualhost que desejar

### Gerenciador ###

* Foi criado um gerenciador para ver as tarefas criadas e alterar as suas ordens.